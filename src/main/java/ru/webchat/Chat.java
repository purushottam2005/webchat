/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.webchat;

import java.util.Collections;
import ru.webchat.entities.Message;
import ru.webchat.entities.User;
import java.util.List;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import ru.webchat.persistence.HibernateUtil;

/**
 *
 * @author Labazin
 */
@ManagedBean
@SessionScoped
public class Chat {
    
    private static final Logger log = Logger.getLogger(Chat.class.getName());
    
    private String message;
    private String viewType;
    private User currentUser;

    /**
     * Creates a new instance of Chat
     */
    public Chat() {
        viewType = "4";
        log.fine("Chat instance has created");
    }
    
    public User getCurrentUser() {
        if (currentUser == null) {
            String nick = FacesContext.getCurrentInstance()
                    .getExternalContext().getUserPrincipal().getName();
            if (nick == null) {
                log.severe("Chat instance doesn't have user info");
                return null;
            }
            Session session = HibernateUtil.getSessionFactory().openSession();
            currentUser = (User) session.createCriteria(User.class)
                    .add(Restrictions.eq("nick", nick)).uniqueResult();
            session.close();
            log.fine("Chat has found out user \"" + nick + "\"");
        }
        return currentUser;
    }
    
    public void setViewType(String viewType) {
        this.viewType = viewType;
    }
    
    public String getViewType() {
        return viewType;
    }
    
    public void setMessage(String message) {
        this.message = message;
    }
    
    public String getMessage() {
        return message;
    }
    
    public void sendMessage() {
        log.info("User " + currentUser.getNick() + " has sent message " + message);
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        Message msg = new Message(getCurrentUser(), message.toString());
        getCurrentUser().getMessages().add(msg);
        session.save(msg);
        session.getTransaction().commit();
        session.close();
        message = "";
    }
    
    public List<Message> getAllMessages() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Criteria criteria = session.createCriteria(Message.class);
        List<Message> result = null;
        if ("4".equals(viewType)) {
            result = criteria.addOrder(Order.desc("id")).list();
        } else {
            int maxResult = 0;
            if ("1".equals(viewType)) {
                maxResult = 50;
            } else if ("2".equals(viewType)) {
                maxResult = 100;
            } else if ("3".equals(viewType)) {
                maxResult = 200;
            }
            result = criteria.addOrder(Order.desc("id"))
                    .setMaxResults(maxResult).list();
        }
        Collections.reverse(result);
        session.close();
        return result;
    }
    
    public String logout() {
        log.info("User " + currentUser.getNick() + " has logout");
        String result = "/login?faces-redirect=true";
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        try {
            request.logout();
        } catch (ServletException e) {
            log.severe("User " + currentUser.getNick() + " can't logout");
        }
        return result;
    }
}
