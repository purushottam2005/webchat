package ru.webchat;

import java.util.Map;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import ru.webchat.entities.User;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import ru.webchat.entities.UserGroup;
import ru.webchat.persistence.HibernateUtil;

/**
 *
 * @author Labazin
 */
@ManagedBean
public class Registration {
    
    private static final Logger log = Logger.getLogger(Registration.class.getName());

    private User user;
    
    /**
     * Creates a new instance of Registration
     */
    public Registration() {
        user = new User();
        log.fine("Registration instance has created");
    }
    
    @PostConstruct
    public void init() {
        FacesContext context = FacesContext.getCurrentInstance();
        Map<String,String> params = context
                .getExternalContext().getRequestParameterMap();
        if (params.containsKey("j_password") && params.containsKey("j_username")) {
            log.warning("User has flunked authorization.\n" 
                    + "username: " + params.get("j_username") 
                    + "; password: " + params.get("j_password"));
            context.addMessage(null, new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, 
                    "Ошибка аунтетификации. Логин или пароль были заданы неправильно.",
                    ""));
        }
    }
    
    public User getUser() {
        return user;
    }
    
    public String registrate() {
        String returnPage = null; // if null, stay on the same page
        FacesMessage message;
        Session session = HibernateUtil.getSessionFactory().openSession();
        
        // Check if user with this nick-name already exists
        Object obj = session.createCriteria(User.class)
                .add(Restrictions.eq("nick", user.getNick())).uniqueResult();
        if (obj != null) {
            log.warning("User has wanted to set his nick name to \"" 
                    + user.getNick() + "\", but it's already in use");
            message = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "Ошибка регистрации",
                    "Пользователь с именем " + user.getNick()
                            + " уже зарегестрирован в системе.");
        } else {
            session.beginTransaction();
            // Check if a default group for all users exists
            Object groupObj = session.createCriteria(UserGroup.class)
                    .add(Restrictions.eq("name", "user")).uniqueResult();
            if (groupObj == null) {
                log.warning("Default group USER doesn't exist");
                groupObj = new UserGroup("user");
            }
            
            user.getGroups().add((UserGroup) groupObj);
            session.save(user);
            session.getTransaction().commit();
            log.fine("User " + user.getNick() + " has successfully created");
            message = new FacesMessage(
                    FacesMessage.SEVERITY_INFO, 
                    "Успешная регистрация",  
                    "Регистрация нового пользователя прошла успешно.");
            returnPage = "login?faces-redirect=true";
        }
        session.close();
        FacesContext.getCurrentInstance().addMessage(null, message);
        return returnPage;
    }
}
