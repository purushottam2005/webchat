package ru.webchat.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.logging.Logger;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.ocpsoft.prettytime.PrettyTime;

/**
 *
 * @author Labazin
 */
@Entity
@Table(name="messages", catalog="public")
@org.hibernate.annotations.Entity(
		dynamicInsert = true
)
public class Message implements Serializable {
    
    private static Logger log = Logger.getLogger(Message.class.getName());
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id", nullable=false, columnDefinition="serial")
    private Integer id;
    
    @Column(name="message_time", nullable=true,
            columnDefinition="timestamp without time zone DEFAULT now()")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date date;
    
    @Column(name="message_text", nullable=false)
    private String text;
    
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="user_id", nullable=false)
    private User user;
    
    public Message() {
    }
    
    public Message(User user, String text) {
        this.user = user;
        this.text = text;
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }
    
    public String getFormatDate() {
        return new PrettyTime().format(date);
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
}
