package ru.webchat.entities;

import java.io.Serializable;
import java.util.Set;
import java.util.logging.Logger;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 *
 * @author Labazin
 */
@Entity
@Table(name="groups", catalog="public")
public class UserGroup implements Serializable {
    
    private static Logger log = Logger.getLogger(UserGroup.class.getName());
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", nullable=false, columnDefinition = "serial")
    private Integer id;
    
    @Column(name="group_name", nullable=false)
    private String name;
    
    @ManyToMany(fetch=FetchType.EAGER, mappedBy="groups")
    private Set<User> user;
    
    @Column(name="description")
    private String description;
    
    public UserGroup() {
    }
    
    public UserGroup(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<User> getUser() {
        return user;
    }

    public void setUser(Set<User> user) {
        this.user = user;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getDescription() {
        return description;
    }
    
}
