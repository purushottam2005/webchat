/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.webchat.entities;

import java.io.Serializable;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Labazin
 */
@Entity
@Table(name="users", catalog="public")
public class User implements Serializable {
    
    private static Logger log = Logger.getLogger(User.class.getName());
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", nullable=false, columnDefinition = "serial")
    private Integer id;
    
    @Column(name="nick_name", nullable=false, length=25)
    private String nick;
    
    @Column(name="first_name", nullable=false, length=25)
    private String firstName;
    
    @Column(name="middle_name", nullable=false, length=25)
    private String middleName;
    
    @Column(name="last_name", nullable=false, length=25)
    private String lastName;
    
    @Column(name="email", nullable=false, length=255)
    private String email;
    
    @Column(name="password", nullable=false, length=32)
    private String password;
    
    @OneToMany(fetch=FetchType.EAGER, mappedBy="user")
    private Set<Message> messages;
    
    @ManyToMany(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinTable(name="users_groups", catalog="public",
        joinColumns={@JoinColumn(name="user_id", nullable=false, updatable=false)}, 
        inverseJoinColumns={@JoinColumn(name="group_id", nullable=false, updatable=false) })
    private Set<UserGroup> groups;
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    public Integer getId() {
        return id;
    }
    
    public void setNick(String nick) {
        this.nick = nick;
    }
    
    public String getNick() {
        return nick;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public void setPassword(String password) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.reset();
        md.update(password.getBytes(Charset.forName("UTF8")));
        StringBuilder sb = new StringBuilder();
        for (byte md5Byte : md.digest()) {
            sb.append(Integer.toHexString(
                    (md5Byte & 0xFF) | 0x100).subSequence(1, 3));
        }
        this.password = sb.toString();
    }
    
    public String getPassword() {
        return password;
    }

    public Set<Message> getMessages() {
        if (messages == null) {
            messages = new HashSet<Message>();
        }
        return messages;
    }

    public void setMessages(Set<Message> messages) {
        this.messages = messages;
    }

    public Set<UserGroup> getGroups() {
        if (groups == null) {
            groups = new HashSet<UserGroup>();
        }
        return groups;
    }

    public void setGroups(Set<UserGroup> groups) {
        this.groups = groups;
    }
    
}
