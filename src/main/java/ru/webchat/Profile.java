/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.webchat;

import java.security.NoSuchAlgorithmException;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import ru.webchat.entities.User;
import ru.webchat.persistence.HibernateUtil;

/**
 *
 * @author Labazin
 */
@ManagedBean
public class Profile {
    
    private static final Logger log = Logger.getLogger(Profile.class.getName());
    
    private String newPassword;
    private User user;

    /**
     * Creates a new instance of Profile
     */
    public Profile() {
        log.fine("Profile instance has created");
    }
    
    @PostConstruct
    public void init() {
        String nick = FacesContext.getCurrentInstance()
                .getExternalContext().getRemoteUser();
        if (nick == null) {
            log.severe("Profile doesn't have any user");
            return;
        }
        log.fine("Profile loaded for user with nick name \"" + nick + "\"");
        Session session = HibernateUtil.getSessionFactory().openSession();
        user = (User) session.createCriteria(User.class)
                .add(Restrictions.eq("nick", nick)).uniqueResult();
        session.close();
        log.fine("Profile loaded for user " + user.getLastName() + ' ' 
                + user.getFirstName() + ' ' + user.getMiddleName());
    }
    
    public void setNewPassword(String newPassword) {
        log.fine("User want to change his password to " + newPassword);
        this.newPassword = newPassword;
    }
    
    public String getNewPassword() {
        return newPassword;
    }
    
    public User getUser() {
        return user;
    }
    
    public void restore() {
        log.fine("User restores his profile by default");
        Session session = HibernateUtil.getSessionFactory().openSession();
        user = (User) session.createCriteria(User.class).add(
                Restrictions.eq("id", user.getId())).uniqueResult();
        session.close();
    }
    
    public void save() throws NoSuchAlgorithmException {
        FacesMessage message;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Object obj = session.createCriteria(User.class)
                .add(Restrictions.eq("nick", user.getNick())).uniqueResult();
        if (obj != null) {
            log.warning("User has wanted to change his nick name to \"" 
                    + user.getNick() + "\", but it's already in use");
            message = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR,
                    "Ошибка смены ника",
                    "Пользователь с именем " + user.getNick()
                            + " уже зарегестрирован в системе.");
        } else {
            session.beginTransaction();
            if (newPassword != null) {
                user.setPassword(newPassword);
            }
            session.update(user);
            session.getTransaction().commit();
            log.fine("User successfully saved profile's changes");
            message = new FacesMessage(
                    FacesMessage.SEVERITY_INFO, 
                    "Данные успешно изменены",  
                    "Изменения данных пользователя прошли успешно.");
        }
        session.close();
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
}
